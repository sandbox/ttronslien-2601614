<?php

/**
 * @file
 * Generates paths based on tokens and association with group.
 */

/**
 * Implements hook_node_update().
 */
function og_multiple_paths_create_alias($node, $op, $source = '', $data = '', $type = NULL, $language = LANGUAGE_NONE) {

  module_load_include('inc', 'pathauto');
  $pattern = pathauto_pattern_load_by_entity("node", $node->type);

  if (empty($pattern)) {
    /* No pattern? Do nothing otherwise we may blow away existing aliases. */
    return '';
  }

  /* Special handling when updating an item which is already aliased. */
  $existing_alias = NULL;
  if ($op == 'update' || $op == 'bulkupdate') {
    if ($existing_alias == _pathauto_existing_alias_data($source, $language)) {
      switch (variable_get('pathauto_update_action', PATHAUTO_UPDATE_ACTION_DELETE)) {
        case PATHAUTO_UPDATE_ACTION_NO_NEW:
          /* If an alias already exists, and the update action is set to do nothing. */
          return '';
      }
    }
  }

  /* Replace any tokens in the pattern. Uses callback option to clean replacements. No sanitization. */
  $alias = token_replace($pattern, array('node' => $node), array(
    'sanitize' => FALSE,
    'clear' => TRUE,
    'callback' => 'pathauto_clean_token_values',
    'language' => (object) array('language' => $language),
    'pathauto' => FALSE,
  ));

  /* @see token_scan() */
  $pattern_tokens_removed = preg_replace('/\[[^\s\]:]*:[^\s\]]*\]/', '', $pattern);
  if ($alias === $pattern_tokens_removed) {
    return '';
  }

  /* Deletes existing aliases if any. */
  pathauto_path_delete_all('node/' . $node->nid);

  /* Loop through all group fields as checked of in the configuration */
  $array_og_fields = variable_get('og_multiple_paths_config_fields');
  foreach ($array_og_fields as $og_field) {
    og_multiple_paths_generate_path($node, $og_field, $alias, $language);
  }
  return TRUE;
}

/**
 * Custom function generating and saving the path. Used by create_alias().
 */
function og_multiple_paths_generate_path($node, $field, $alias, $language) {
  $group_wrapper = entity_metadata_wrapper('node', $node);

  if (isset($group_wrapper->$field)) {
    foreach ($group_wrapper->$field->value() as $elements) {
      $group_path = drupal_get_path_alias("node/" . $elements->nid);
      $new_alias = str_replace('[node:og-multipath]', $group_path, $alias);
      /* Build the new path alias array and send it off to be created. */
      $path = array(
        'source' => 'node/' . $node->nid,
        'alias' => $new_alias,
        'language' => $language,
      );
      path_save($path);
    }
  }
}
