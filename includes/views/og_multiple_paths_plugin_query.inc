<?php

/**
 * @file
 * Page Callback: A fake pager plugin that wraps around the actual query.
 *
 * The theme system allows for nearly all output of the Drupal system.
 * To be customized by user themes.
 */

/**
 * Class. Perform any needed actions just after the query executing.
 */
class OgMultiplePathsPluginQuery extends OgMultiplePathsPluginWrapper {

  /**
   * Executes the passed view.
   */
  public function execute(&$view) {

    $pager = new OgMultiplePathsPluginPager();
    $pager->ogMultipathWrap($this->wrapped->pager);

    $this->wrapped->execute($view);

    $pager->ogMultipathUnwrap();
  }

}
