<?php

/**
 * @file
 * Og_multiple_paths admin settings.
 */

/**
 * Admin form settings.
 */
function og_multiple_paths_admin_settings($form_state) {

  /* Calling the list of fieldentityreferences */
  $array_field_options = og_multiple_paths_get_entityref_fields();

  /* The drupal checkboxes form field definition */
  $form['og_multiple_paths_config_fields'] = array(
    '#title' => t('Organic Group Fields'),
    '#description' => t('This is a list of entity reference fields found on your site. Select the fields used on nodes that contain the association between a node and groups.'),
    '#type' => 'checkboxes',
    '#options' => $array_field_options,
    '#default_value' => variable_get('og_multiple_paths_config_fields', FALSE),
  );

  return system_settings_form($form);
}
