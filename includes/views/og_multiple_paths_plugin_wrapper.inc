<?php

/**
 * @file
 * A helper class that wraps around the actual views plugin.
 *
 * @see OgMultiplePathsPluginQuery
 * @see og_multiple_paths_plugin_pager
 */

/**
 * Class. Wraps and unwraps paging links.
 */
class OgMultiplePathsPluginWrapper {

  protected $wrapped;
  protected $wrappedlink;

  /**
   * Wrapper.
   */
  public function ogMultipathWrap(&$link) {
    $this->wrappedlink = &$link;
    $this->wrapped = $link;
    $link = $this;
  }

  /**
   * Unwrapper.
   */
  public function ogMultipathUnwrap() {
    $this->wrappedlink = $this->wrapped;
    unset($this->wrapped);
    unset($this->wrappedlink);
  }

  /**
   * Getter.
   */
  public function &__get($name) {
    return $this->wrapped->$name;
  }

  /**
   * Setter.
   */
  public function __set($name, $value) {
    return $this->wrapped->$name = $value;
  }

  /**
   * Isset.
   */
  public function __isset($name) {
    return isset($this->wrapped->$name);
  }

  /**
   * Unsetting.
   */
  public function __unset($name) {
    unset($this->wrapped->$name);
  }

  /**
   * Function call.
   */
  public function __call($name, $arguments) {
    return call_user_func_array(array($this->wrapped, $name), $arguments);
  }

  /**
   * Call Static. As of PHP 5.3.0.
   */
  public static function __callStatic($name, $arguments) {
    return call_user_func_array(array(get_class($this->wrapped), $name), $arguments);
  }

}
