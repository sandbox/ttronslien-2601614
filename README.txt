-- SUMMARY --

This module creates multiple paths for the same node using Organic Groups and
Pathauto. It’s not best practice for SEO. However, it addresses issues with
usability and content management in settings where Organic Groups have unique
themes and navigation while sharing content.

For a full description of the module, visit the sandbox:
  https://www.drupal.org/sandbox/ttronslien/2601614

To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/2601614

-- REQUIREMENTS --

Organic Groups (tested with 7.x-2.7)
Pathauto (tested with 7.x-1.2)

-- RECOMMENDED MODULE --

Views
Metatag (or simlilar module)

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.

-- CONFIGURATION --

Create content types and groups as per Organic Group instructions.

Next, go to Configure, under Organic Groups, you should now see
OG Multiple Pathauto Settings. Click on it. Select the field that should
be used to create the path structure. This is the field
that is added to a content type and that creates an OG reference,
such as og_group_ref

View
The default view is configured to accept the field og_group_ref that is the
default Organic Group Reference field. The default view will look for the group
on the current node - thus current node can only be associated with one group -
pass the "Current OG Group from Content" (type node) to the view and use the
path from the group to generate the path for the content pulled into the view.

How it works
Views & Group on Node
E.G. you have a group called Bobcats, which is on the path teams/bobcats.
Then you'd like news views that show news stories on the same path.
However, the news story may be shared with multiple teams.

Assumptions:
Basic Page has a group field.
Content type "News" has a group field.

Create a basic page news, associated it with the team Bobcats
(You may want to configure Pathauto to create paths for your groups such that
the news page is teams/bobcats/news.)
Create a views block news, pull in all news content types. In the context field,
insert the Organic Group field used on the Basic Content type and create a
relation to "OG Membership from node". Save.

Insert the news block on the news page. You should now see that the paths of the
news stories are all on the path with Bobcats regardless of what other groups
associated with the news stories.

Views & Panels
Same as above but rather than insert the Organic Group field in context, add
"Global" and pass in the group id via the panel.

Configure Canonical to reduce SEO Impact (Thank you Drupal user mmowers)
By using a SEO module you can configure canonical URLs. This will insert
<link rel="canonical" href="ORIGINAL URL" /> in the header of your pages.

-- TROUBLESHOOTING --

Broken/missing handler (Default View)
The default view will throw Broken/missing handler. You will have add OG group
field to a content type. Add the OG gropu field to the views Contextual filter,
and then Configure the Relationship.


-- CONTACT --

Current maintainers:
* Tove Tronslien (ttronslien) - http://www.drupal.org/user/1634428/

This project has been sponsored by:
* OPIN Enterprise Drupal
  http://www.opin.ca for more information.
