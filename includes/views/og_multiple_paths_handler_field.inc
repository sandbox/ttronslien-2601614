<?php

/**
 * @file
 * A handler to provide a views field.
 */

/**
 * Class. Views Field Handler.
 */
class OgMultiplePathsHandlerField extends views_handler_field {

  /**
   * Implements hook views_pre_render.
   */
  public function ogMultipathPreExecute() {
  }

  /**
   * Manipulates the query.
   */
  public function query() {
    /* Provide an field alias but don't actually alter the query. */
    $this->field_alias = 'og_paths_' . $this->position;
    /* Inform _views_pre_execute() to seize control over the query. */
    $this->view->og_multiple_paths = TRUE;
  }

  /**
   * Executes before render.
   */
  public function preRender(&$values) {
  }

  /**
   * The rendering of the custom field handler.
   */
  public function render($values) {
    $group = $this->view->args[0];
    $aliases = db_query('
      SELECT alias
      FROM {url_alias}
      WHERE source = :source', array(':source' => 'node/' . $group))->fetchAll();

    $group = $aliases[0]->alias;

    $aliases = db_query('
      SELECT alias
      FROM {url_alias}
      WHERE source = :source AND alias like :alias ORDER BY Length(alias) asc LIMIT 1', array(':source' => 'node/' . $values->nid, ':alias' => '%' . db_like($group) . '%'))->fetchAll();

    /* if no alias found return default drupal alias */
    if (isset($aliases[0]->alias)) {
      return $aliases[0]->alias;
    }
    else {
      return drupal_get_path_alias('node/' . $values->nid);
    }
  }

  /**
   * Handling after the query execute.
   */
  public function ogMultipathPostExecute() {
    /* Ecexute value PHP code. */
  }

}
