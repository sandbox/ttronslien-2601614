<?php

/**
 * @file
 * Config and Processing of the og-multipath token to be used in pathauto.
 */

/**
 * Implements hook_token_info().
 */
function og_multiple_paths_token_info() {
  $info['tokens']['node']['og-multipath'] = array(
    'name' => t('OG Multipath'),
    'description' => t('Generates paths based on the groups associated with node.'),
  );
  return $info;
}


/**
 * Implements hook_tokens().
 *
 * Parses the token and replace it with it's value.
 */
function og_multiple_paths_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  if ($type == 'node' && !empty($data['node'])) {
    $node = $data['node'];

    foreach ($tokens as $name => $original) {
      if ($options['pathauto'] == FALSE && $name == 'og-multipath') {
        $replacements[$original] = $original;
      }
      if ($options['pathauto'] == TRUE && $name == 'og-multipath') {
        $array_field_options = og_multiple_paths_get_entityref_fields();
        foreach ($array_field_options as $value) {
          if (isset($node->$value)) {
            $fields = $node->$value;
            $parent = array_pop($fields[LANGUAGE_NONE]);
            $replacements[$original] = drupal_get_path_alias("node/" . $parent['target_id']);
          }
        }
      }
    }
  }
  return $replacements;
}
