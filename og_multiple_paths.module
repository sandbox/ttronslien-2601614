<?php

/**
 * @file
 * Generates multiple paths using pathauto for one node based on organic groups.
 */

module_load_include('inc', 'og_multiple_paths', 'includes/og_multiple_paths');
module_load_include('inc', 'og_multiple_paths', 'includes/og_multiple_paths.tokens');
module_load_include('inc', 'og_multiple_paths', 'includes/og_multiple_paths.views');

/**
 * Implements hook_menu().
 */
function og_multiple_paths_menu() {
  $items['admin/config/group/multipaths'] = array(
    'title' => 'OG Multiple Pathauto Settings',
    'description' => 'Configure OG Pathauto settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('og_multiple_paths_admin_settings'),
    'access arguments' => array('administer group'),
    'file' => 'includes/og_multiple_paths.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_node_update().
 */
function og_multiple_paths_node_update($node) {
  og_multiple_paths_create_alias($node, 'update');
}

/**
 * Implements hook_node_insert().
 */
function og_multiple_paths_node_insert($node) {
  og_multiple_paths_create_alias($node, 'insert');
}

/**
 * Implements hook_pathauto_bulkupdate().
 */
function og_multiple_paths_bulkupdate($context) {
  /* Find all node ids for the store content type. */
  $query = db_select('node', 'n');
  $query
    ->condition('n.type', 'article')
    ->fields('n', array('nid'));
  $results = $query->execute()->fetchCol();
  $count = 0;
  foreach ($results as $nid) {
    $node = node_load($nid);
    og_multiple_paths_create_alias($node, 'bulkupdate');
    $count++;
  }
  drupal_set_message(t($count . ' node extras were updated.'));
}

/**
 * Get fields of entity reference.
 *
 * The type used to connect node with boolean group=true to other entities.
 */
function og_multiple_paths_get_entityref_fields() {
  $query = db_select('field_config', 'fc');
  $query->fields('fc', array('field_name'))
    ->condition('fc.type', 'entityreference', '=');
  $og_fields = $query->execute();

  foreach ($og_fields as $elements) {
    $array_field_options[$elements->field_name] = $elements->field_name;
  }

  return $array_field_options;
}

/**
 * Implements hook_views_api().
 */
function og_multiple_paths_views_api() {
  return array(
    'api' => 3,
  );
}

/**
 * Implements hook_views_pre_execute().
 */
function og_multiple_paths_views_pre_execute($view) {
  /* Seize control over the query plugin if requested. */
  $query = new OgMultiplePathsPluginQuery();
  $query->ogMultipathWrap($view->query);
}

/**
 * Implements hook_views_post_execute().
 */
function og_multiple_paths_views_post_execute($view) {
  /* Restore original query plugin if it was wrapped. */
  if ($view->query instanceof OgMultiplePathsPluginWrapper) {
    $view->query->ogMultipathUnwrap();
  }
}
